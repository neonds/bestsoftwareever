package com.cruxconsultores;

import com.cruxconsultores.interfaces.NamingImpl;

public class Main {

	public Main() {		
	}
	
	public static void main(String[] args) {
		
		//LLama al nombre
		NamingImpl name = new Guillermo();
		NamingImpl names = new Josh();
		name.showYourName();
		
		//Llama otra vez al nombre
		name.showYourName();
		
		//Llama por tercera vez el metodo
		name.showYourName();
		
		names.showYourName();		
	}
}
