package com.cruxconsultores;

import com.cruxconsultores.interfaces.NamingImpl;

public class Josh implements NamingImpl {

	@Override
	public void showYourName() {
		System.out.println("My Name Is Joshua");		
	}
}
